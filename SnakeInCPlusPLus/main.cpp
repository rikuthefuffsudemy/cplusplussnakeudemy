#include <iostream>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>

#define FRECCIA_SU 72
#define FRECCIA_GIU 80
#define FRECCIA_SINISTRA 75
#define FRECCIA_DESTRA 77

using namespace std;
bool gameOver;
const int larghezza = 20;
const int altezza = 20;
int playerX;
int playerY;
int punteggio;
int fruttoX;
int fruttoY;

int lunghezzaCoda;
int codaX[50];
int codaY[50];

enum eDirezione
{
	STOP = 0,
	SINISTRA,
	DESTRA,
	SU,
	GIU
};
eDirezione direzioneCorrente;

void MostraCursore(bool mostra);

void Setup()
{
	srand((unsigned)(time(NULL)));
	gameOver = false;
	playerX = larghezza / 2;
	playerY = altezza / 2;
	punteggio = 0;
	fruttoX = rand() % larghezza;
	fruttoY = rand() % altezza;
	direzioneCorrente = STOP;
	lunghezzaCoda = 0;
	MostraCursore(false);
}

void PulisciSchermo()
{
	HANDLE gestoreOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD coordinateCursore;
	coordinateCursore.X = 0;
	coordinateCursore.Y = 0;
	SetConsoleCursorPosition(gestoreOutput, coordinateCursore);
	//system("cls");
}

void MostraCursore(bool mostra)
{
	HANDLE gestoreOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO informazioniCursore;
	GetConsoleCursorInfo(gestoreOutput, &informazioniCursore);
	informazioniCursore.bVisible = mostra;
	SetConsoleCursorInfo(gestoreOutput, &informazioniCursore);
}

void Visual()
{
	PulisciSchermo();

	//bordo superiore
	for (int i = 0; i < larghezza; i++) //i++ -> i = i + 1
	{
		cout << "#";
	}
	cout << endl;

	//arena
	for (int i = 0; i < altezza; i++)
	{
		for (int j = 0; j < larghezza; j++)
		{
			if ((i == playerY) && (j == playerX))
			{
				cout << "O";
			}
			else if ((i == fruttoY) && (j == fruttoX))
			{
				cout << "*";
			}
			else
			{
				bool hoStampatoLaCoda = false;
				for (int k = 0; k < lunghezzaCoda; k++)
				{
					if ((j == codaX[k]) && (i == codaY[k]))
					{
						cout << "o";
						hoStampatoLaCoda = true;
					}
				}
				if (!hoStampatoLaCoda)
				{
					if ((j == 0) || (j == larghezza - 1))
					{
						cout << "#";
					}
					else
					{
						cout << " ";
					}
				}
			}
		}
		cout << endl;
	}


	//bordo inferiore
	for (int i = 0; i < larghezza; i++) //i++ -> i = i + 1
	{
		cout << "#";
	}
	cout << endl;
	cout << "Punteggio: " << punteggio << endl;
}

void Logic()
{
	for (int i = lunghezzaCoda - 1; i > 0; i--)
	{
		codaX[i] = codaX[i - 1];
		codaY[i] = codaY[i - 1];
	}

	codaX[0] = playerX;
	codaY[0] = playerY;

	switch (direzioneCorrente)
	{
	case STOP: break;
	case SU:
		playerY--; //playerY = player + 1;
		break;
	case GIU: playerY++; break;
	case DESTRA: playerX++; break;
	case SINISTRA: playerX--; break;
	default: break;
	}

	if ((playerX == fruttoX) && (playerY == fruttoY))
	{
		punteggio++;
		lunghezzaCoda++;
		fruttoX = rand() % larghezza;
		fruttoY = rand() % altezza;
	}
	else
	{
		for (int i = 0; i < lunghezzaCoda; i++)
		{
			if ((playerX == codaX[i]) && (playerY == codaY[i]))
			{
				gameOver = true;
				return;
			}
		}
	}

	if (playerX >= larghezza)
	{
		playerX = 0;
	}
	else if (playerX < 0)
	{
		playerX = larghezza - 1;
	}
	if (playerY >= altezza)
	{
		playerY = 0;
	}
	else if (playerY < 0)
	{
		playerY = altezza - 1;
	}
}

void Input()
{
	if (_kbhit())
	{
		char tastoPremuto = _getch(); //codice ASCII del carattere premuto
		switch (tastoPremuto)
		{
		case 'w':
		case FRECCIA_SU:
			direzioneCorrente = SU;
			break;
		case 'a':
		case FRECCIA_SINISTRA:
			direzioneCorrente = SINISTRA;
			break;
		case 's':
		case FRECCIA_GIU:
			direzioneCorrente = GIU;
			break;
		case 'd':
		case FRECCIA_DESTRA:
			direzioneCorrente = DESTRA;
			break;
		case 'x':
			gameOver = true;
			break;
		default: break;
		}
	}
}

int main()
{
	Setup();

	while (!gameOver)
	{
		Visual();
		Input();
		Logic();
		Sleep(50);
	}

	system("pause");
	return 0;
}